import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second-v2',
  template: `
    <p>
      second-v2 works!
    </p>
  `,
  styles: []
})
export class SecondV2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
