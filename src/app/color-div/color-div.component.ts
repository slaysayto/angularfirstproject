import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-color-div',
  templateUrl: './color-div.component.html',
  styleUrls: ['./color-div.component.css']
})
export class ColorDivComponent implements OnInit {
  colour: 'blue';
  constructor() { }

  ngOnInit() {
  }

  changeColor(MyInput: HTMLInputElement) {
    console.log('hi');
    // @ts-ignore
    this.colour = MyInput.value;
  }
}
